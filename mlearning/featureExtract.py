'''
Created on 10.10.2017
@author: davidgraf
description:
'''

from mlearning.contentFeatures import *
from mlearning.userFeatures import *
from nlproc.preprocessing import *
from mlearning.featureEval import featureClassCoerr
from inout.writeData import *
from sklearn.feature_selection import VarianceThreshold,SelectKBest
import sklearn.preprocessing as pp
import re

def featureExtraction(crisisEvent):
    event = crisisEvent.getDesc().get("name").replace(" ","")
    category = crisisEvent.getDesc().get("categorization").get("category")
    subcategory = crisisEvent.getDesc().get("categorization").get("sub_category")
    eventtype = crisisEvent.getDesc().get("categorization").get("type")
    timedevelopment = crisisEvent.getDesc().get("time").get("development")
    timeduration = crisisEvent.getDesc().get("time").get("duration")
    country = crisisEvent.getDesc().get("location").get("country")
    spread = crisisEvent.getDesc().get("location").get("spread")
    locationdescription = crisisEvent.getDesc().get("location").get("location_description").replace(","," ")
    featureMatrix = []
    labelMatrix = []
    features = ["numberOfTokens",
                "numberOfHash",
                "numberOfQMark",
                "numberOfEMark",
                "numberOfURL",
                "numberOfAt",
                "sourceMedia",
                "sourceNGO",
                "sourceGov",
                "sourceEyew",
                "sourceBus",
                "sourceOut",
                "numberOfNouns",
                "numberOfVerbs",
                "numberOfAdj",
                "numberOfAdv",
                "posEmoji",
                "negEmoji",
                "sentimentpolarity",
                "sentimentsubjectivity",
                #"retweetcount",
                "replyTimeToEvent",
                "numberOfContainedTags",
                "negationWordsCount",
                #"regTimeToEvent",
                "langEN",
                "langES",
                "langTL",
                "langPT",
                "finishesWithPunct",
                #"NumberOfFriends",
                #"NumberOfFollowers",
                #"NumberOfFav",
                #"IsGeoEnabled",
                #"IsVerified",
                #"mobileClient",
                #"desktopClient",
                #"userInteractions",
                #"isInTimeZoneOfEvent"
                ]
    # for export to csv and feature analysis
    statMatrix = [["event", "id", "timestamp", "informativeness", "category", "sub-category", "eventtype", "timedevelopment", "timeduration", "country", "spread", "location_description"]+features]

    for tweet in crisisEvent.getListOfTweets():
        #add informativeness as label
        labelMatrix.append(tweet.getInformativeness())

        #remove special characters
        cleanTweetText = re.sub(r'[^\x00-\x7f]', r' ', tweet.getText())

        featureVector = ([numberOfTokens(tweet.getText()),
                          numberOfHash(tweet.getText()),
                          numberOfQMark(tweet.getText()),
                          numberOfEMark(tweet.getText()),
                          numberOfURL(tweet.getText()),
                          numberOfAt(tweet.getText()),
                          sourceMedia(tweet.getInfoSource()),
                          sourceNGO(tweet.getInfoSource()),
                          sourceGov(tweet.getInfoSource()),
                          sourceEyew(tweet.getInfoSource()),
                          sourceBus(tweet.getInfoSource()),
                          sourceOut(tweet.getInfoSource()),
                          numberOfTag(posTagger(tokenizer(tweet.getText())),'NN'),
                          numberOfTag(posTagger(tokenizer(tweet.getText())), 'VB'),
                          numberOfTag(posTagger(tokenizer(tweet.getText())), 'JJ'),
                          numberOfTag(posTagger(tokenizer(tweet.getText())), 'RB'),
                          posEmoji(tweet.getText()),
                          negEmoji(tweet.getText()),
                          sentimentAnalysis(cleanTweetText,0),
                          sentimentAnalysis(cleanTweetText,1),
                          #retweetCount(tweet),
                          replyTimeToSource(crisisEvent.getDesc().get("time"), tweet.getTimestamp()),
                          numberOfContainedTags(tweet.getText(), crisisEvent.getDesc().get("keywords")),
                          negationWordsCount(tweet.getText()),
                          #regTimeToEvent(crisisEvent.getDesc().get("time"), tweet.getUser().getCreatdat()),
                          langDetect(cleanTweetText, "en"),
                          langDetect(cleanTweetText, "es"),
                          langDetect(cleanTweetText, "tl"),
                          langDetect(cleanTweetText, "pt"),
                          finishesWithPunct(tweet.getText()),
                          #NumberOfFriends(tweet.getUser()),
                          #NumberOfFollowers(tweet.getUser()),
                          #NumberOfFav(tweet.getUser()),
                          #IsGeoEnabled(tweet.getUser()),
                          #IsVerified(tweet.getUser()),
                          #mobileClient(tweet.getSource()),
                          #desktopClient(tweet.getSource()),
                          #userInteractions(tweet.getUser(),crisisEvent.getListOfTweets()),
                          #isInTimeZone(crisisEvent.getDesc().get("timezone"),tweet.getUser())
                      ])

        featureMatrix.append(featureVector[:len(features)])  # number of features

        statMatrix.append([event,tweet.getId(),tweet.getTimestamp(),tweet.getInformativeness(),category,subcategory,eventtype,timedevelopment,timeduration,country,spread,locationdescription]+featureVector[:len(features)])


    # standardization (zero mean, variance of one)
    stdScaler = pp.StandardScaler().fit(featureMatrix)
    featureMatrixScaled = stdScaler.transform(featureMatrix)

    # min-max scaler
    #minmaxScaler = pp.MinMaxScaler(copy=True, feature_range=(0, 1)).fit(featureMatrix)
    #featureMatrixScaled = minmaxScaler.transform(featureMatrix)

    # feature evaluation
    featureClassCoerr(featureMatrixScaled, labelMatrix, features)

    # write feature statistics to csv
    writeToCSV(statMatrix,"stat_"+event+".csv")

    return featureSelection(featureMatrixScaled), labelMatrix, statMatrix

def featureSelection(featureMatrix):

    #sel = VarianceThreshold(threshold=(0.2)) # Variance Treshold > 0.2
    #featureMatrixSelected = sel.fit_transform(featureMatrix)

    return featureMatrix
