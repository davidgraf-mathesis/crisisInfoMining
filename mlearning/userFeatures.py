'''
Created on 10.10.2017
@author: davidgraf
description:
'''

import datetime as dt

def regTimeToEvent(sourceDate, regDate):
    if (sourceDate == None or regDate == None):
        return 0
    else:
        startDay = dt.datetime.strptime(sourceDate.get("start_day"), '%Y-%m-%d')
        timeDelta = startDay - regDate
        return timeDelta.days

def NumberOfFriends(user):
    if user.getFriendscount() == None:
        return 0
    else:
        return user.getFriendscount()

def NumberOfFollowers(user):
    if user.getFollowerscount() == None:
        return 0
    else:
        return user.getFollowerscount()

def NumberOfFav(user):
    if user.getFavouritescount() == None:
        return 0
    else:
        return user.getFavouritescount()

def IsGeoEnabled(user):
    if user.getIsgeoenabled():
        return 1
    else:
        return 0

def IsVerified(user):
    if user.getIsverified():
        return 1
    else:
        return 0

def mobileClient(link):
    if link == None:
        return 0
    else:
        if link.find("Twitter for Android") != -1:
            return 1
        if link.find("Twitter for iP") != -1:
            return 1
        if link.find("Twitter for Blackberry") != -1:
            return 1
        if link.find("Mobile") != -1:
            return 1
        else:
            return 0

def desktopClient(link):
    if link == None:
        return 0
    else:
        if link.find("Twitter Web Client") != -1:
            return 1
        if link.find("Twitter for Mac") != -1:
            return 1
        if link.find("Twitter for Websites") != -1:
            return 1
        else:
            return 0

def userInteractions(user, tweets):
    if user.getName() == None:
        return 0
    else:
        count = 0

        for tweet in tweets:
            if tweet.getUser().getName() == user.getName():
                count = count + 1

        return count - 1  # because self matching

def isInTimeZone(timezone,user):
    if timezone == None or user.getTimezone() == None:
        return 0
    else:
        for tz in timezone:
            if tz == user.getTimezone():
                return 1
        return 0
