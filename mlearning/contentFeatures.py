'''
Created on 10.10.2017
@author: davidgraf
description:
'''

from nlproc.preprocessing import tokenizer,stemming,remStopwords,posTagger
import re
import datetime as dt
import time as time
from langdetect import detect_langs
from textblob import TextBlob

def numberOfTokens(tweetText):
    return len(tokenizer(tweetText))

def numberOfHash(tweetText):
    return len(re.findall("#", tweetText))

def numberOfQMark(tweetText):
    return len(re.findall("\?", tweetText))

def numberOfEMark(tweetText):
    return len(re.findall("!", tweetText))

def numberOfURL(tweetText):
    return len(re.findall("http:", tweetText))

def numberOfAt(tweetText):
    return len(re.findall("@", tweetText))

def sourceMedia(tweetSource):
    if tweetSource == 'Media':
        return 1
    else:
        return 0

def sourceNGO(tweetSource):
    if tweetSource == 'NGOs':
        return 1
    else:
        return 0

def sourceGov(tweetSource):
    if tweetSource == 'Government':
        return 1
    else:
        return 0

def sourceEyew(tweetSource):
    if tweetSource == 'Eyewitness':
        return 1
    else:
        return 0

def sourceBus(tweetSource):
    if tweetSource == 'Business':
        return 1
    else:
        return 0

def sourceOut(tweetSource):
    if tweetSource == 'Outsiders':
        return 1
    else:
        return 0

def numberOfTag(taggedList,tag):
    count = 0
    for t in taggedList:
        if t[1][:2] == tag:
            count=count+1
    return count

def posEmoji(tweetText):
    return len(re.findall(":\)", tweetText)) \
           + len(re.findall(":-\)", tweetText))

def negEmoji(tweetText):
    return len(re.findall(":\(", tweetText)) \
           + len(re.findall(":-\(", tweetText))

def retweetCount(tweet):
    if (tweet.getRetweetcount() == None):
        return 0
    else:
        return tweet.getRetweetcount()

def replyTimeToSource(sourceDate, replyDate):
    if (sourceDate == None or replyDate == None):
        return (-1)*sourceDate.get("duration") #if no date then -duration
    else:
        startDay = dt.datetime.strptime(sourceDate.get("start_day"), '%Y-%m-%d')
        timeDelta = replyDate - startDay
        return timeDelta.days

def numberOfContainedTags(tweetText,tags):
    count = 0
    for t in tags:
        if len(re.findall(t, tweetText))>0:
            count=count+1
    return count

def negationWordsCount(tweetText):
    NEGATIONWORDS = ["not","none","neither","never","no one","nobody","nor","nothing","nowhere","does not","did not","fuck"]
    count = 0

    for w in NEGATIONWORDS:
        count += len(re.findall(w, tweetText))

    return count

def langEN(tweet):
    if tweet.getLang() == "en":
        return 1
    else:
        return 0

def langES(tweet):
    if tweet.getLang() == "es":
        return 1
    else:
        return 0

def langTL(tweet):
    if tweet.getLang() == "tl":
        return 1
    else:
        return 0

def langPT(tweet):
    if tweet.getLang() == "pt":
        return 1
    else:
        return 0

def finishesWithPunct(tweetText):
    lastChar = tweetText[-1]
    if lastChar == "." or lastChar == "?" or lastChar == "!":
        return 1
    else:
        return 0

def langDetect(tweetText, detectLang):
    try:
        langs = detect_langs(tweetText)
    except:
        return 0
    for l in langs:
        if l.lang == detectLang:
            return l.prob
    return 0

def sentimentAnalysis(tweetText, mode):
    analysis = TextBlob(tweetText)
    if mode == 0:
        return analysis.sentiment.polarity
    elif mode == 1:
        return analysis.sentiment.subjectivity
