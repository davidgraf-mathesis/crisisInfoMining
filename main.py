'''
Created on 10.10.2017
@author: davidgraf
description:
'''

from inout.readData import readDataFromFile, readDataFromDB, readFeaturesFromFile
from inout.writeData import combineCSV, saveFeaturesToFile
from data.processing import balanceData
from data.statistics import PCAandPlot
from mlearning.featureExtract import featureExtraction
from mlearning.classifier import trainModel, testModel
import sklearn.metrics as scikitm
import numpy as np
import os.path
import random
from classes.crisisEvent import *

# ---- K O N F I G U R A T I O N -----

# ------------ M O D E ---------------
# 0 ... default - set Trainevent(s)/Testevent(s) in variables
# 1 ... loop over events (generate features outputs for all events)
MODE = 0
DATA_DIR = "D:/David/Studium/CS/MasterThesis/datasets/results/"

# --------- V A R I A B L E S --------
#TRAINSET = [0,1,2,3,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25]  #[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25]
TRAINSET = [1,3,9]
TESTSET = [2]

# 0	    2012_Colorado_wildfires
# 1	    2012_Costa_Rica_earthquake
# 2	    2012_Guatemala_earthquake
# 3	    2012_Italy_earthquakes
# 4	    2012_Philipinnes_floods
# 5	    2012_Typhoon_Pablo
# 6	    2012_Venezuela_refinery
# 7	    2013_Alberta_floods
# 8	    2013_Australia_bushfire
# 9	    2013_Bohol_earthquake
# 10	2013_Boston_bombings
# 11	2013_Brazil_nightclub_fire
# 12	2013_Colorado_floods
# 13	2013_Glasgow_helicopter_crash
# 14	2013_Lac_Megantic_train_crash
# 15	2013_LA_airport_shootings
# 16	2013_Manila_floods
# 17	2013_NY_train_crash
# 18	2013_Queensland_floods
# 19	2013_Russia_meteor
# 20	2013_Sardinia_floods
# 21	2013_Savar_building_collapse
# 22	2013_Singapore_haze
# 23	2013_Spain_train_crash
# 24	2013_Typhoon_Yolanda
# 25	2013_West_Texas_explosion

UPDATE_FEATURES = True
EXP_CSVCOMBINE = False
PLOT_PCA = False

# --------- S A M P L I N G --------
SAMPLE_DATA = False
SAMPLES = 4000

# --------- B A L A N C I N G --------
BALANCE_DATA = True

# --------- S P L I T T I N G --------
SPLIT_IN_TIMEPHASE = False
PHASE_AUTO = False
# PHASE_EARLY = 0
PHASE_MID = 4
PHASE_LATE = 10
PHASE_LATE_PART = 2

# - E N D - K O N F I G U R A T I O N -

print "------------------------- app start -------------------------"
print "Konfigmode: " + str(MODE)

# read data
data = readDataFromDB()

# get features

# ------------------------- M O D E - 0 --------------------

if MODE == 0:
    print "------------------------- Extract features -------------------------"
    featureMatrixTrainTotal = None
    labelMatrixTrainTotal = None
    statMatrixTrainTotal = None
    featureMatrixTestTotal = None
    labelMatrixTestTotal = None
    statMatrixTestTotal = None
    trainEvents = ""
    testEvents = ""

    # combine data for multiple event training/testing
    for e in TRAINSET:
        TRAIN = data[e]
        trainEvents += TRAIN.getDesc().get("name") + ", "
        print "Trainset: Get features from: " + TRAIN.getDesc().get("name") + " Nr of Teeets: " + str(len(TRAIN.getListOfTweets()))

        # read features from cache
        if UPDATE_FEATURES or not os.path.exists(DATA_DIR+"expfeatures/" + TRAIN.getDesc().get("name").replace(" ", "") + "_features.csv"):
            featureMatrixTrain, labelMatrixTrain, statMatrixTrain = featureExtraction(TRAIN)
            saveFeaturesToFile(TRAIN.getDesc().get("name").replace(" ",""), featureMatrixTrain, labelMatrixTrain, statMatrixTrain)
        else:
            featureMatrixTrain, labelMatrixTrain, statMatrixTrain = readFeaturesFromFile(TRAIN.getDesc().get("name").replace(" ",""))

        if featureMatrixTrainTotal is not None:
            featureMatrixTrainTotal = np.concatenate((featureMatrixTrainTotal, featureMatrixTrain), axis=0)
        else:
            featureMatrixTrainTotal = featureMatrixTrain
        if labelMatrixTrainTotal is not None:
            labelMatrixTrainTotal = np.concatenate((labelMatrixTrainTotal, labelMatrixTrain), axis=0)
        else:
            labelMatrixTrainTotal = labelMatrixTrain
        if statMatrixTrainTotal is not None:
            statMatrixTrainTotal = np.concatenate((statMatrixTrainTotal, statMatrixTrain[1:]), axis=0)
        else:
            statMatrixTrainTotal = statMatrixTrain
    for e in TESTSET:
        TEST = data[e]
        testEvents += TEST.getDesc().get("name") + ", "
        print "Testset: Get features from: " + TEST.getDesc().get("name") + " Nr of Teeets: " + str(len(TEST.getListOfTweets()))

        # read features from cache
        if UPDATE_FEATURES or not os.path.exists(
                                        DATA_DIR + "expfeatures/" + TEST.getDesc().get("name").replace(" ", "") + "_features.csv"):
            featureMatrixTest, labelMatrixTest, statMatrixTest = featureExtraction(TEST)
            saveFeaturesToFile(TEST.getDesc().get("name").replace(" ", ""), featureMatrixTest, labelMatrixTest,
                               statMatrixTest)
        else:
            featureMatrixTest, labelMatrixTest, statMatrixTest = readFeaturesFromFile(
                TEST.getDesc().get("name").replace(" ", ""))

        if featureMatrixTestTotal is not None:
            featureMatrixTestTotal = np.concatenate((featureMatrixTestTotal, featureMatrixTest), axis=0)
        else:
            featureMatrixTestTotal = featureMatrixTest
        if labelMatrixTestTotal is not None:
            labelMatrixTestTotal = np.concatenate((labelMatrixTestTotal, labelMatrixTest), axis=0)
        else:
            labelMatrixTestTotal = labelMatrixTest
        if statMatrixTestTotal is not None:
            statMatrixTestTotal = np.concatenate((statMatrixTestTotal, statMatrixTest[1:]), axis=0)
        else:
            statMatrixTestTotal = statMatrixTest

        if PLOT_PCA:
            PCAandPlot(featureMatrixTrainTotal, labelMatrixTrainTotal, statMatrixTrainTotal[1:, 0])

    # split dataset in time phases
    if SPLIT_IN_TIMEPHASE:

        # list of training sets (early,mid,late in events)
        setSplitTrainFeaturesList = [[],[],[]]
        setSplitTrainLabelList = [[],[],[]]

        # list of test sets (early,mid,late in events)
        setSplitTestFeaturesList = [[],[],[]]
        setSplitTestLabelList = [[],[],[]]

        # get index of reply time column
        if isinstance(statMatrixTrainTotal[0],(list,)):
            replyTimeToEvent = statMatrixTrainTotal[0].index("replyTimeToEvent")
            eventDuration = statMatrixTrainTotal[0].index("timeduration")
        else:
            replyTimeToEvent = statMatrixTrainTotal[0].tolist().index("replyTimeToEvent")
            eventDuration = statMatrixTrainTotal[0].tolist().index("timeduration")

        # pre-initialization
        phaseMid = PHASE_MID
        phaseLate = PHASE_LATE

        # match tweet to correct time phase (training)
        for i in range(len(labelMatrixTrainTotal)):
            # set up time phase interval
            if PHASE_AUTO:
                #phaseMid = 5
                phaseLate = int(statMatrixTrainTotal[i+1][eventDuration]) / PHASE_LATE_PART

            if int(statMatrixTrainTotal[i+1][replyTimeToEvent]) < phaseMid:
                phase = 0
            elif int(statMatrixTrainTotal[i+1][replyTimeToEvent]) <= phaseLate:
                phase = 1
            else:
                phase = 2

            setSplitTrainFeaturesList[phase].append(featureMatrixTrainTotal[i].tolist())
            setSplitTrainLabelList[phase].append(labelMatrixTrainTotal[i])

        # match tweet to correct time phase (testing)
        for i in range(len(featureMatrixTestTotal)):
            # set up time phase interval
            if PHASE_AUTO:
                #phaseMid = 5
                phaseLate = int(statMatrixTestTotal[i+1][eventDuration]) / PHASE_LATE_PART

            if int(statMatrixTestTotal[i+1][replyTimeToEvent]) < phaseMid:
                phase = 0
            elif int(statMatrixTestTotal[i+1][replyTimeToEvent]) <= phaseLate:
                phase = 1
            else:
                phase = 2

            if isinstance(featureMatrixTestTotal[i],(list,)):
                setSplitTestFeaturesList[phase].append(featureMatrixTestTotal[i])
            else:
                setSplitTestFeaturesList[phase].append(featureMatrixTestTotal[i].tolist())
            setSplitTestLabelList[phase].append(labelMatrixTestTotal[i])

        crossValScore = []
        weightedCrossValScore = []
        accurcyScore = []
        weightedAccurcyScore = []
        precisionScore = []
        recallScore = []
        f1Score = []
        TrainBalancedPhasesScore = []
        TestBalancedPhasesScore = []

        # train/test all phases
        for i in range(len(setSplitTrainLabelList)):
            if len(setSplitTrainLabelList[i]) > 1 and len(setSplitTestLabelList[i]) > 1:
                print "------------------------- Train/Test Phase "+str(i)+" -------------------------"
                model, meanCrossVal = trainModel(setSplitTrainFeaturesList[i], setSplitTrainLabelList[i], "")
                crossValScore.append(meanCrossVal)
                weightedCrossValScore.append(meanCrossVal*len(setSplitTrainLabelList[i]))
                accuracy, precision, recall, f1 = testModel(model, setSplitTestFeaturesList[i], setSplitTestLabelList[i], "")
                accurcyScore.append(accuracy)
                weightedAccurcyScore.append(accuracy*len(setSplitTestLabelList[i]))
                precisionScore.append(precision)
                recallScore.append(recall)
                f1Score.append(f1)
                TrainBalancedPhasesScore.append(len(setSplitTrainLabelList[i]))
                TestBalancedPhasesScore.append(len(setSplitTestLabelList[i]))

        print "--------------- Classification Summary ---------------"
        print "Mean Cross-Val: " + str(np.mean(crossValScore))
        print "Weighted mean Cross-Val: " + str(np.sum(weightedCrossValScore)/np.sum(TrainBalancedPhasesScore))
        print "Mean Accuracy: " + str(np.mean(accurcyScore))
        print "Weighted mean Accuracy: " + str(np.sum(weightedAccurcyScore)/np.sum(TestBalancedPhasesScore))
        print "Mean Precision: " + str(np.mean(precisionScore))
        print "Mean Recall: " + str(np.mean(recallScore))
        print "Mean F1: " + str(np.mean(f1Score))
        print "StDev TrainTweets: " + str(np.std(TrainBalancedPhasesScore))
        print "StDev TestTweets: " + str(np.std(TestBalancedPhasesScore))

    # train/test without time phases
    else:
        # use samples
        if (SAMPLE_DATA and len(labelMatrixTrainTotal)>SAMPLES):
            print "--------------------- Sample training dataset ---------------------"
            print "Sample "+str(SAMPLES)+" values (out of "+str(len(labelMatrixTrainTotal))+" values)"
            sampleFeatureMatrixTrainTotal = []
            sampleLabelMatrixTrainTotal = []
            trainEvents = str(SAMPLES)+" samples of " + trainEvents

            sampleIndexList = random.sample(range(0, len(labelMatrixTrainTotal)), SAMPLES)

            for index in sampleIndexList:
                sampleFeatureMatrixTrainTotal.append(featureMatrixTrainTotal[index])
                sampleLabelMatrixTrainTotal.append(labelMatrixTrainTotal[index])

            if BALANCE_DATA:
                balSampleFeatureMatrixTrainTotal, balSampleLabelMatrixTrainTotal = balanceData(sampleFeatureMatrixTrainTotal,sampleLabelMatrixTrainTotal)
                model, meanCrossVal = trainModel(balSampleFeatureMatrixTrainTotal, balSampleLabelMatrixTrainTotal, trainEvents)
            else:
                model, meanCrossVal = trainModel(sampleFeatureMatrixTrainTotal, sampleLabelMatrixTrainTotal, trainEvents)
        else:
            if BALANCE_DATA:
                balFeatureMatrixTrainTotal, balLabelMatrixTrainTotal = balanceData(featureMatrixTrainTotal,labelMatrixTrainTotal)
                model, meanCrossVal = trainModel(balFeatureMatrixTrainTotal, balLabelMatrixTrainTotal, trainEvents)
            else:
                model, meanCrossVal = trainModel(featureMatrixTrainTotal, labelMatrixTrainTotal, trainEvents)

        accuracy, precision, recall, f1 = testModel(model, featureMatrixTestTotal, labelMatrixTestTotal, testEvents)


# ------------------------- M O D E - 1 --------------------

if MODE == 1: # generate statistic files
    for e in data:
        if len(e.getListOfTweets()) > 0:
            print "Extract features: " + e.getDesc().get("name") + " Nr of Teeets: " + str(len(e.getListOfTweets()))
            featureExtraction(e)

print "------------------------- Statistics -------------------------"
if EXP_CSVCOMBINE:
    combineCSV()

print "------------------------- app finished -------------------------"



