'''
Created on 10.10.2017
@author: davidgraf
description:
'''

#class definitions
class Tweet:
    id = 0
    timestamp = ''
    text = ''
    infoSource = ''
    infoType = ''
    informativeness = ''
    retweetId = 0
    lang = ''
    retweetcount = 0
    source = ''
    dataset = ''
    user = None

    def __init__(self, id, timestamp, text, iS, iT, iN, retweetId, lang, retweetcount, source, dataset, user):
        self.id = id
        self.timestamp = timestamp
        self.text = text
        self.infoSource = iS
        self.infoType = iT
        self.retweetId = retweetId
        self.lang = lang
        self.retweetcount = retweetcount
        self.source = source
        self.dataset = dataset
        self.user = user

        # convert informativeness into integer
        if iN == "Not related\n" or iN == "Not related":
            self.informativeness = 0
        elif iN == "Related - but not informative\n" or iN == "Related - but not informative":
            self.informativeness = 0
        elif iN == "Related and informative\n" or iN == "Related and informative":
            self.informativeness = 1
        else:
            self.informativeness = 0

    def getId(self):
        return self.id

    def getTimestamp(self):
        return self.timestamp

    def getText(self):
        return self.text

    def getInfoSource(self):
        return self.infoSource

    def getInfoType(self):
        return self.infoType

    def getInformativeness(self):
        return self.informativeness

    def getDataset(self):
        return self.dataset

    def getRetweetcount(self):
        return self.retweetcount

    def getUser(self):
        return self.user

    def getSource(self):
        return self.source

    def getLang(self):
        return self.lang