'''
Created on 10.10.2017
@author: davidgraf
description:
'''

#class definitions
class CrisisEvent:
    desc = ''
    ListOfTweets = []

    def __init__(self, desc):
        self.desc = desc

    def setDesc (self, d):
        self.desc = d

    def getDesc (self):
        return self.desc

    def getListOfTweets (self):
        return self.ListOfTweets

    def getKeywords (self):
        return self.desc.get('keywords')