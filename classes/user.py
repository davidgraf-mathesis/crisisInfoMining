'''
Created on 16.01.2018
@author: davidgraf
description:
'''

#class definitions
class User:
    id = 0
    favouritescount = 0
    followerscount = 0
    friendscount = 0
    lang = ''
    listedcount = 0
    location = ''
    name = ''
    screenname = ''
    statusescount = ''
    timezone = ''
    url = ''
    isfollowrequestsent = False
    isgeoenabled = False
    isverified = False
    createdat = ''

    def __init__(self, id, favouritescount, followerscount, friendscount, lang, listedcount, location, name, screenname, statusescount, timezone, url, isfollowrequestsent, isgeoenabled, isverified, createdat):
        self.id = id
        self.favouritescount = favouritescount
        self.followerscount = followerscount
        self.friendscount = friendscount
        self.lang = lang
        self.listedcount = listedcount
        self.location = location
        self.name = name
        self.screenname = screenname
        self.statusescount = statusescount
        self.timezone = timezone
        self.url = url
        self.isfollowrequestsent = isfollowrequestsent
        self.isgeoenabled = isgeoenabled
        self.isverified = isverified
        self.createdat = createdat

    def getCreatdat(self):
        return self.createdat

    def getFriendscount(self):
        return self.friendscount

    def getFollowerscount(self):
        return self.followerscount

    def getFavouritescount(self):
        return self.favouritescount

    def getIsgeoenabled(self):
        return self.isgeoenabled

    def getIsverified(self):
        return self.isverified

    def getName(self):
        return self.name

    def getTimezone(self):
        return self.timezone