
from nltk.corpus import stopwords
from nltk import pos_tag, word_tokenize
from nltk.stem.porter import PorterStemmer

def tokenizer(tweetText):
    try:
        return word_tokenize(tweetText)
    except:
        return set("")

def remStopwords(tweetText):
    stopTerms = set(stopwords.words("english"))
    tokens = set(tweetText.split()).difference(stopTerms)
    return tokens

def stemming(tweetText):
    stemmer = PorterStemmer()
    stemmedTerms = set([])

    try:
        for term in tweetText:
            stemmedTerm = stemmer.stem(term)
            stemmedTerms.add(str(stemmedTerm))

        return stemmedTerms
    except:
        return tweetText

def posTagger(tweetText):
    try:
        return pos_tag(tweetText)
    except:
        return [()]






