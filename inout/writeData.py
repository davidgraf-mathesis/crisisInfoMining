'''
Created on 10.10.2017
@author: davidgraf
description:
'''

import os
import numpy as np
import pandas as pd
import glob

DATA_DIR = "D:/David/Studium/CS/MasterThesis/datasets/results/"

def writeToDB (data):
    print 'not yet implemented'






def writeToCSV (matrix, filename):

    myfile = open(DATA_DIR + filename, "w")
    for l in matrix:
        try:
            if isinstance(l, (list,)) or isinstance(l, (np.ndarray,)):
                for v in l:
                    myfile.write(str(v) + ";")
            else:
                myfile.write(str(l))
            myfile.write("\n")
        except:
            myfile.write("Error in writing file "+filename)

    myfile.close()

    print "data exported to " + filename

def combineCSV ():

    os.chdir(DATA_DIR)
    try:
        os.remove(DATA_DIR+"stat_combined.csv")
    except:
        print "file does not exist"
    combined = pd.DataFrame([])

    for counter, file in enumerate(glob.glob("stat_*")):
        content = pd.read_csv(file)
        combined = combined.append(content)

    combined.to_csv(DATA_DIR+"stat_combined.csv",index=False)

    print "csv files combined for statistics"

def saveFeaturesToFile (event,featureMatrix,labelMatrix,statMatrix):
    writeToCSV(featureMatrix, "expfeatures/" + event + "_features.csv")
    writeToCSV(labelMatrix, "expfeatures/" + event + "_labels.csv")
    writeToCSV(statMatrix, "expfeatures/" + event + "_statistics.csv")