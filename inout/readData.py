'''
Created on 10.10.2017
@author: davidgraf
description:
'''

# imports
import json
import os
import psycopg2

from classes.crisisEvent import *
from classes.tweet import *
from classes.user import *

# variables
DATA_DIR = "D:/David/Studium/CS/MasterThesis/datasets/CrisisLexT26-v1.0/CrisisLexT26/"
USE_EXTENDED = False

def readDataFromFile():

    # create List of Crisis Events
    crisisEventsList = []
    ListOfTweets = []

    # read data
    crisisDir = os.listdir(DATA_DIR)

    for dir in crisisDir:

        path = DATA_DIR + dir

        if os.path.isdir(path):

            # create new CrisisEvent
            newCrisisEvent = CrisisEvent('')


            # read event description
            with open(path + '/' + dir + '-event_description.json') as event_desc:
                json_event_desc = json.load(event_desc)
                event_desc.close()

            newCrisisEvent.setDesc(json_event_desc)

            # read labeled tweets
            for line in open(path + '/' + dir + '-tweets_labeled.csv'):
                readLine = line.split(",")
                if readLine[0]!="Tweet ID": # skip header line
                    ListOfTweets.append(Tweet(readLine[0], '', readLine[1], readLine[2], readLine[3], readLine[4], 0, '', 0, '', '', None))

            print path + ' read'
            newCrisisEvent.ListOfTweets = ListOfTweets
            ListOfTweets = []
            crisisEventsList.append(newCrisisEvent)

    print 'data read finished'

    return crisisEventsList


def readDataFromDB():

    conn = psycopg2.connect("dbname='crisisLexJKU' user='postgres' host='localhost' password='postgres'")
    print "DB Connection successful"

    cur = conn.cursor()

    # create List of Crisis Events
    crisisEventsList = []
    ListOfTweets = []

    # read data
    crisisDir = os.listdir(DATA_DIR)

    for dir in crisisDir:

        path = DATA_DIR + dir

        if os.path.isdir(path):

            # create new CrisisEvent
            newCrisisEvent = CrisisEvent('')

            # read event description
            with open(path + '/' + dir + '-event_description.json') as event_desc:
                json_event_desc = json.load(event_desc)
                event_desc.close()

            newCrisisEvent.setDesc(json_event_desc)

            # read labeled tweets

            if USE_EXTENDED:
                cur.execute("""select 
                                t.text,t.infosource,t.infotype,t.informativeness,t.dataset, 
                                timezone('UTC', c.timestamp),s.retweetid,s.lang,s.retweetcount,s.source,s.tweetid, 
                                u.id,u.favouritescount,u.followerscount,u.friendscount,u.lang,u.listedcount,u.location,u.name,u.screenname,u.statusescount,u.timezone,u.url,u.isfollowrequestsent,u.isgeoenabled,u.isverified,u.createdat 
                            from public.labeledTweet t 
                            join public.collectedtweet c on t.tweetid = c.tweetid 
                            join public.Status s on t.tweetid = s.tweetid 
                            join public."User" u on s.userid = u.id
                            where t.dataset = %s""", (dir,))

                tweets = cur.fetchall()

                for tweet in tweets:
                    ListOfTweets.append(
                        Tweet(tweet[10], tweet[5], tweet[0], tweet[1], tweet[2], tweet[3], tweet[6], tweet[7], tweet[8],
                              tweet[9], tweet[4],
                              User(tweet[11], tweet[12], tweet[13], tweet[14], tweet[15], tweet[16], tweet[17],
                                   tweet[18], tweet[19], tweet[20], tweet[21], tweet[22], tweet[23], tweet[24],
                                   tweet[25], tweet[26])))

            else:
                cur.execute("""select 
                               t.text,t.infosource,t.infotype,t.informativeness,t.dataset, 
                               timezone('UTC', c.timestamp),c.tweetid 
                            from public.labeledTweet t 
                            join public.collectedtweet c on t.tweetid = c.tweetid 
                            where t.dataset = %s""", (dir,))

                tweets = cur.fetchall()

                for tweet in tweets:
                    ListOfTweets.append(
                        Tweet(tweet[6], tweet[5], tweet[0], tweet[1], tweet[2], tweet[3], 0, '', 0,
                              '', tweet[4], None))

            print path + ' read'
            newCrisisEvent.ListOfTweets = ListOfTweets
            ListOfTweets = []
            crisisEventsList.append(newCrisisEvent)

    print 'data read finished'

    return crisisEventsList

def readFeaturesFromFile(event):

    features = []
    labels = []
    statistics = []

    for line in open("D:/David/Studium/CS/MasterThesis/datasets/results/expfeatures/"+ event + "_features.csv"):
        readLine = line.split(";")
        features.append(map(float, readLine[:-1]))  # remove last empty cell

    for line in open("D:/David/Studium/CS/MasterThesis/datasets/results/expfeatures/"+ event + "_labels.csv"):
        labels.append(int(line.replace("\n", "")))  # remove last empty cell

    for line in open("D:/David/Studium/CS/MasterThesis/datasets/results/expfeatures/"+ event + "_statistics.csv"):
        readLine = line.split(";")
        statistics.append(readLine[:-1])  # remove last empty cell

    return features, labels, statistics











