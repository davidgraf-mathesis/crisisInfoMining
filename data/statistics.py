'''
Created on 10.10.2017
@author: davidgraf
description:
'''

import numpy as np
from sklearn.decomposition import PCA
import matplotlib.pyplot as plt

ONLY_POS = True

def PCAandPlot(featureMatrix,labelMatrix,eventList):

    print "--------------------- PCA Plot -----------------------"

    if ONLY_POS:
        PCAFeatureMatrix = []
        PCAEventMatrix = []

        for i in range(len(labelMatrix)):
            if labelMatrix[i] == 1:
                PCAFeatureMatrix.append(featureMatrix[i])
                PCAEventMatrix.append(eventList[i])

        PCAFeatureMatrix = np.asarray(PCAFeatureMatrix)
        PCAEventMatrix = np.asarray(PCAEventMatrix)
        print "Number of positive values only: " + str(len(PCAFeatureMatrix))
    else:
        PCAFeatureMatrix = featureMatrix
        PCAEventMatrix = eventList
        print "Number of positive and negative values: " + str(len(PCAFeatureMatrix))

    pca = PCA(n_components=2)
    reduced_pca = pca.fit_transform(PCAFeatureMatrix)
    reduced_pca.shape

    events = np.unique(PCAEventMatrix).tolist()
    colors = ['black', 'blue', 'purple', 'yellow', 'white', 'red', 'lime', 'cyan', 'orange', 'gray',
              'green', 'magenta', 'darkgreen', 'orange', 'gold', 'indigo', 'lightblue', 'navy', 'salmon', 'tomato'
        , 'wheat', 'violet', 'turquoise', 'peachpuff', 'papayawhip', 'slategray']

    for i in range(len(events)):
        x = reduced_pca[:, 0][PCAEventMatrix == events[i]]
        y = reduced_pca[:, 1][PCAEventMatrix == events[i]]
        plt.scatter(x, y, c=colors[i], s=2)
    plt.legend(events)
    plt.xlabel('First Principal Component')
    plt.ylabel('Second Principal Component')
    plt.title("informative Tweets")
    plt.show()