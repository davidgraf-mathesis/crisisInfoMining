'''
Created on 10.10.2017
@author: davidgraf
description:
'''

import random
import numpy as np

SAMPLE_UNDER = False
SAMPLE_OVER = True

def balanceData (featureMatrix, labelMatrix):
    print "------------------------- Balance Dataset -------------------------"
    posFeatureMatrix = []
    posLabelMatrix = []
    negFeatureMatrix = []
    negLabelMatrix = []
    sampleFeatureMatrix = []
    sampleLabelMatrix = []
    #pos = len(filter(lambda x: x == 1, labelMatrix))
    #neg = len(filter(lambda x: x == 0, labelMatrix))

    for i in range(len(labelMatrix)):
        if labelMatrix[i] == 1:
            posFeatureMatrix.append(featureMatrix[i])
            posLabelMatrix.append(labelMatrix[i])
        else:
            negFeatureMatrix.append(featureMatrix[i])
            negLabelMatrix.append(labelMatrix[i])

    if (SAMPLE_UNDER):
        if (len(posLabelMatrix) > len(negLabelMatrix)):
            sampleIndexList = random.sample(range(0, len(posLabelMatrix)), len(negLabelMatrix))

            for index in sampleIndexList:
                sampleFeatureMatrix.append(posFeatureMatrix[index])
                sampleLabelMatrix.append(posLabelMatrix[index])

            sampleFeatureMatrix = np.concatenate((sampleFeatureMatrix, negFeatureMatrix), axis=0)
            sampleLabelMatrix = np.concatenate((sampleLabelMatrix, negLabelMatrix), axis=0)
            print "Under sampling: Reduce positive to "+str(len(negLabelMatrix))+" values (out of "+str(len(posLabelMatrix))+" values)"

        elif(len(posLabelMatrix) < len(negLabelMatrix)):
            sampleIndexList = random.sample(range(0, len(negLabelMatrix)), len(posLabelMatrix))

            for index in sampleIndexList:
                sampleFeatureMatrix.append(negFeatureMatrix[index])
                sampleLabelMatrix.append(negLabelMatrix[index])

            sampleFeatureMatrix = np.concatenate((sampleFeatureMatrix, posFeatureMatrix), axis=0)
            sampleLabelMatrix = np.concatenate((sampleLabelMatrix, posLabelMatrix), axis=0)
            print "Under sampling: Reduce negative to " + str(len(posLabelMatrix)) + " values (out of "+str(len(posLabelMatrix))+" values)"

    if (SAMPLE_OVER):
        if (len(posLabelMatrix) > len(negLabelMatrix)):
            sampleIndexList = random.sample(range(0, len(negLabelMatrix)), (len(posLabelMatrix)-len(negLabelMatrix)))

            for index in sampleIndexList:
                sampleFeatureMatrix.append(negFeatureMatrix[index])
                sampleLabelMatrix.append(negLabelMatrix[index])

            sampleFeatureMatrix = np.concatenate((sampleFeatureMatrix, negFeatureMatrix), axis=0)
            sampleFeatureMatrix = np.concatenate((sampleFeatureMatrix, posFeatureMatrix), axis=0)
            sampleLabelMatrix = np.concatenate((sampleLabelMatrix, negLabelMatrix), axis=0)
            sampleLabelMatrix = np.concatenate((sampleLabelMatrix, posLabelMatrix), axis=0)
            print "Over sampling: Add "+str(len(sampleIndexList))+" negative sample-copies to " + str(len(negLabelMatrix)) + " values (in total "+ str(len(posLabelMatrix))+" negative samples)"

        elif (len(posLabelMatrix) < len(negLabelMatrix)):
            sampleIndexList = random.sample(range(0, len(posLabelMatrix)), (len(negLabelMatrix)-len(posLabelMatrix)))

            for index in sampleIndexList:
                sampleFeatureMatrix.append(posFeatureMatrix[index])
                sampleLabelMatrix.append(posLabelMatrix[index])

            sampleFeatureMatrix = np.concatenate((sampleFeatureMatrix, posFeatureMatrix), axis=0)
            sampleFeatureMatrix = np.concatenate((sampleFeatureMatrix, negFeatureMatrix), axis=0)
            sampleLabelMatrix = np.concatenate((sampleLabelMatrix, posLabelMatrix), axis=0)
            sampleLabelMatrix = np.concatenate((sampleLabelMatrix, negLabelMatrix), axis=0)
            print "Over sampling: Add " + str(len(sampleIndexList)) + " positive-sample copies to " + str(
                len(negLabelMatrix)) + " values (in total " + str(len(posLabelMatrix)) + " positive samples)"

    return sampleFeatureMatrix, sampleLabelMatrix